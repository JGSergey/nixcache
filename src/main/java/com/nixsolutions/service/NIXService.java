package com.nixsolutions.service;

import com.nixsolutions.model.NIX;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

@CacheConfig(cacheNames = "NIXCache")
@Component("nixService")
public class NIXService {

    @CachePut(key = "#name")
    public NIX createNIX(String name) {
        System.out.println("createNix : " + name);
        return getExpensiveNIX(name);
    }

    @Cacheable(key = "#name")
    public NIX getNIX(String name) {
        System.out.println("getNIX : " + name);
        return getExpensiveNIX(name);
    }

    @CacheEvict
    public void removeNIX(String name) {
    }

    public static NIX getExpensiveNIX(String name){
        System.out.println("getExpensiveNIX : " + name );
        String megaNIXName = name.replaceAll("[Nn]{1}[Ii]{1}[Xx]{1}","MegaNIX");

        NIX nix = new NIX();
        nix.setName(megaNIXName);

        return nix;
    }



}
