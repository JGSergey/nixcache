package com.nixsolutions;

import com.nixsolutions.service.NIXService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ConfigurableApplicationContext;

@EnableCaching
@SpringBootApplication
public class NIXCacheApplication {

	public static void main(String[] args) {
		SpringApplication.run(NIXCacheApplication.class, args);
		ConfigurableApplicationContext context = SpringApplication.run(NIXCacheApplication.class, args);

		NIXService nixService = (NIXService) context.getBean("nixService");

		//createNix
		nixService.createNIX("MyNiX");

		//get
		System.out.println(nixService.getNIX("MyNiX").getName());
		System.out.println(nixService.getNIX("MyNiX").getName() + "\n");

		//remove
		nixService.removeNIX("MyNiX");

		//get
		System.out.println(nixService.getNIX("MyNiX").getName());

	}

}
