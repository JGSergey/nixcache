package com.nixsolutions.config;

import com.github.benmanes.caffeine.cache.CacheLoader;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.nixsolutions.service.NIXService;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

@Configuration
@EnableCaching
public class CacheConfig {

    private final LoadingCache<Object, Object> cache = Caffeine.newBuilder()
            .maximumSize(10_000)
            .expireAfterWrite(10, TimeUnit.MINUTES)
            .build(key -> NIXService.getExpensiveNIX((String)key));

    @Bean
    public CacheLoader cacheLoader (){
        return new CacheLoader() {
            @Override
            public Object load(Object o) throws Exception {
                return cache.get(o);
            }
        };
    }
}
